﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private int playerTurn; // 0 = x player and 1 = o player
    private int turnCounter; // counter the number of turns played
    [SerializeField] private GameObject xPlayerSprite; // gameobject to show when it's x player's turn
    [SerializeField] private GameObject oPlayerSprite; // gameobject to show when it's o player's turn
    public Sprite[] playerIcons;// players icons that will be displayed in the buttons, 0 = x player, 1 = o player
    public Button[] ticTacToeSpaces; // playable spaces in the game
    public int[] markedSpaces;// ID's which spaces have been marked by which player
    [SerializeField] private GameObject winnerPanel;// shows which player won
    [SerializeField] private Text winnerText;

    // Start is called before the first frame update
    void Start()
    {
        GameSetup();
    }

    // used for setting up the game and reseting to replay the game
   public void GameSetup()
    {
        playerTurn = 0;
        turnCounter = 0;
        winnerPanel.gameObject.SetActive(false);
        oPlayerSprite.gameObject.SetActive(false);

        // using a loop to reset all the buttons
        for (int i = 0; i < ticTacToeSpaces.Length; i++)
        {
            ticTacToeSpaces[i].interactable = true;
            ticTacToeSpaces[i].GetComponent<Image>().sprite = null;
        }

        // reseting all the button values
        for (int i =0; i < markedSpaces.Length; i++)
        {
            markedSpaces[i] = -10;
        }
    }

    // place this fuction on each of the playable buttons
    public void TtcButton(int whichNum)
    {
      // setting the button to the player's image and setting the button to uninteractable
        ticTacToeSpaces[whichNum].image.sprite = playerIcons[playerTurn];
        ticTacToeSpaces[whichNum].interactable = false;

        markedSpaces[whichNum] = playerTurn + 1;
        turnCounter++;

        // starts at 4 because before this you cant have a winner
        if (turnCounter > 4)
        {
            WinnerCheck();
        }

        if(playerTurn == 0 )
        {
            playerTurn = 1;
            oPlayerSprite.gameObject.SetActive(true);
            xPlayerSprite.gameObject.SetActive(false);
        }
        else
        {
            playerTurn = 0;
            xPlayerSprite.gameObject.SetActive(true);
            oPlayerSprite.gameObject.SetActive(false);
        }
    }

    private void WinnerCheck()
    {
        int s1 = markedSpaces[0] + markedSpaces[1] + markedSpaces[2];
        int s2 = markedSpaces[3] + markedSpaces[4] + markedSpaces[5];
        int s3 = markedSpaces[6] + markedSpaces[7] + markedSpaces[8];
        int s4 = markedSpaces[0] + markedSpaces[3] + markedSpaces[6];
        int s5 = markedSpaces[1] + markedSpaces[4] + markedSpaces[7];
        int s6 = markedSpaces[2] + markedSpaces[5] + markedSpaces[8];
        int s7 = markedSpaces[0] + markedSpaces[4] + markedSpaces[8];
        int s8 = markedSpaces[6] + markedSpaces[4] + markedSpaces[2];

        var solutions = new int[] { s1, s2, s3, s4, s5, s6, s7, s8 };

        for (int i = 0; i < solutions.Length; i++)
        {
            // we get the values to check winner using the markedspaces array indexs
            if (solutions[i] == 3) // checks if x player has won
            {
                winnerPanel.gameObject.SetActive(true);
                winnerText.text = " x player wins!";
                return;
            }
            else if(solutions[i] == 6) // checks if o player has won
            {
                winnerPanel.gameObject.SetActive(true);
                winnerText.text = " o player wins!";
                return;
            }
            else if (turnCounter == 9) // if nobody has won at this point then it's a draw
            {
                winnerPanel.gameObject.SetActive(true);
                winnerText.text = "Draw!";
                return;
            }
          
        }
    }

  
}
